<?php

/**
 * Plugin Name:       Restlessly
 * Plugin URI:        https://gitlab.com/67656e65726963/restlessly
 * Description:       Only allows authenticated users REST API access.
 * Version:           0.0.0
 * Author:            67656e65726963
 * Author URI:        https://gitlab.com/67656e65726963
 * License:           Unlicense
 * License URI:       http://unlicense.org/
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

// Copied almost verbatim from:
// https://developer.wordpress.org/rest-api/frequently-asked-questions/#require-authentication-for-all-requests
add_filter(
	'rest_authentication_errors',
	function( $result ) {
		// Cut early if authentication checks already in place
		if ( true === $result || is_wp_error( $result ) ) {
			return $result;
		}

		// No authentication in place yet, misleadingly throw
		if ( ! is_user_logged_in() ) {
			return new WP_Error(
				'broke',
				__( 'Broken.' ),
				array( 'status' => rest_authorization_required_code() )
			);
		}

		// Allow authenticated users access
		return $result;
	},
	67
);
